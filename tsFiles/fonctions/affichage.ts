
let btAjouterClient = document.querySelector("#ajouterClient")! as HTMLButtonElement
let divAjouterClient = document.querySelector(".divAjoutClient")! as HTMLDivElement
let divProfilClient = document.querySelector("#profilClient")! as HTMLDivElement
let divCadreInfoClient = document.querySelector("#cadreInfosClient")! as HTMLDivElement

let profilClient = document.querySelector("#profilClient")! as HTMLDivElement
let divCommandes = document.querySelector("#commandes")! as HTMLDivElement
let divMessages = document.querySelector("#messages")! as HTMLDivElement

let tBodyCommandesClient = document.querySelector("#tbodyCommande")! as HTMLDivElement



function afficherTousClients(liste: Client[]) {

    let n: number = 1
    for (let LeClient of liste) {
        let tBody = document.querySelector("#lesClients")!
        tBody.innerHTML += `
            <tr>
            <th scope="row">${n}</th>
            <td>${LeClient.nom}</td>
            <td>${LeClient.prenom}</td>
            <td>${LeClient.email}</td>
            <td>${LeClient.societe}</td>
            <td><button id="afficherDetailClient" value="${LeClient.id}">Afficher</button></td>
            <td><button id="modifierClient" value="${LeClient.id}">Modifier</button></td>
            <td><button id="deleteClient" value="${LeClient.id}">Supprimer</button></td>
            </tr>`

        n++
    }
    console.log("affichage des boutons")
    
}

function afficherDetailClient(id: string, data: Object) { // infos affichées au niveau des liens Profil, commandes
    //console.log(data)
    let _id: number = +id // Number
    let listeObjetsCommandes

    divCadreInfoClient.style.display = "block"

    for (let el of data) { // j'obtiens la liste des articles du clients

        if (+(el.id) == _id) {

            listeObjetsCommandes = el.commands // et non data.el.commands

        }
    }

    console.log("")
    for (let produit of listeObjetsCommandes) {

        //console.log("article : " + produit.article_id)


    }
    listeTousAchatClientAJour(+id, listeObjetsCommandes)
    afficherProfilClient(_id)
    afficherCommandes(_id)
}

btAjouterClient.addEventListener("click", () => {
    
    divAjouterClient.style.display = "block"
})

function apparenceLink(nodeLink:HTMLLinkElement,listNodeLink:any[]) {

    for(let link of listNodeLink){
        link.className = "nav-link"
    }
    nodeLink.className = "nav-link active"
}

function afficherProfilClient(id:number) {
    let client = recupereClient(id)
    profilClient.innerHTML += `
    <p></p> 
    <p>Nom : ${client.nom} </p>
    <p>Prénom : ${client.prenom} </p>
    <p>Date inscription : ${client.dateInscription}</p>
    <p>Date dernière commande : ${client.dateDernierCommande}</p>
    `
    
}

function afficherDivLinkClique(link:HTMLLinkElement) {
    
    let laSection:string = link.type
    
    if(laSection == "profil") {
        profilClient.style.display = "block"
        divCommandes.style.display = "none"
        divMessages.style.display = "none"
    }
    else if (laSection == "commandes") {
        
        profilClient.style.display = "none"
        divCommandes.style.display = "block"
        divMessages.style.display = "none"
        
        
    }
    else if (laSection == "messages") {
        profilClient.style.display = "none"
        divCommandes.style.display = "none"
        divMessages.style.display = "block"
    }
}

function afficherCommandes(id:number){
    let client:Client = recupereClient(id)
    let listCommandes = client.listeTousArticlesIdAchete
    let n = 1

    fetch(lienProduits)
        .then(response => {
            return response.json()

        }).then(data => {  // data des produits de db.json produits

            lesProduits = data
            
            
            for (let idd of listCommandes){
                
                for (let leProduit of lesProduits){
                    if (idd == leProduit.id){
                        tBodyCommandesClient.innerHTML += `
                        <tr>
                            <th scope="row">${n}</th>
                            
                            <td>${leProduit.nom}</td>
                            <td>${leProduit.prix} €</td>
                            
                            
                            <td><button id="afficherDetailProduit" value="">Afficher</button></td>
                            
                        </tr>
                        `
                        n++
                    }
                }

                
            }
            
        })
    
    
    
    
}


    



