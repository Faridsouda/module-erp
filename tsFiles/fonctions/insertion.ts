
let btEnregistrerClient = document.querySelector("#btEnregistrerClient")! as HTMLButtonElement
let caseSociete = document.querySelector("#estSociete")! as HTMLInputElement


function listeTousAchatClientAJour(id: number, listeObjetsCommandes: string[]): void {

    let monClient: Client
    let trouve: boolean = false

    for (let leClient of listeClients) {
        if (leClient.id === id) {
            monClient = leClient
            trouve = true
        }
    }

    if (trouve == false) throw new Error("Pas de client avec cette id")

    for (let produit of listeObjetsCommandes) {


        monClient.listeTousArticlesIdAchete.push(produit.article_id)
    }

    //console.log("listeObjetsCommandes")
    //console.log(listeObjetsCommandes)
    //console.log("monClient")
    console.log(monClient)

}


let infosProduit = () => {


    let lienInfosArticle = "http://localhost:3000/produits"
    let trouve: boolean = false
    let _obj: Object = {} // ici est stocker les infos du produit en Object
    let id: number = 1

    fetch(lienInfosArticle)
        .then(response => {
            return response.json()

        }).then(data => {

            /*console.log("")
            console.log("data produits")
            console.log(data)*/

            for (let obj of data) {
                if (obj.id === id) {
                    trouve = true
                    _obj = obj
                }
            }

            if (trouve == false) throw new Error("l'id ne correspond à aucun produit")


        })
}

async function postData(data: Object = {}) {

    const response = await fetch("http://localhost:3000/users", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })

    return response.json()
}


async function updateData(id: number, data: Object = {}) {

    const response = await fetch("http://localhost:3000/users/" + id, {
        method: 'PATCH',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })

    return response.json()
}

async function deleteData(id: number, data: Object = {}) {

    const response = await fetch("http://localhost:3000/users/" + id, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })

    return response.json()
}

function recupereNouveauClient():Object {
    

    let nom = document.querySelector("#nom")! as HTMLInputElement
    let prenom = document.querySelector("#prenom")! as HTMLInputElement
    let email = document.querySelector("#email")! as HTMLInputElement
    let societe = document.querySelector("#Societe")! as HTMLInputElement
    let nomSociete:string= ""

    let estSociete:string
    if(caseSociete.checked) {
        estSociete = "on";
        if(societe.value == "") nomSociete = "non spécifié";
        else nomSociete = societe.value
    }
    else {
        estSociete = "off"
        nomSociete = "/"
    }

    const leNouveauClient = {
        nom : nom.value,
        prenom : prenom.value,
        email : email.value,
        Societe : nomSociete, //societe.value,
        siSociete: estSociete
    }

    return leNouveauClient
}



    


btEnregistrerClient.addEventListener("click", () => {
    
    let divEnregistrerSucces = document.querySelector("#notificationClientEnregistre")! as HTMLDivElement
    
    objetClient()
    
    if (btEnregistrerClient.innerHTML == "Enregistrer"){
        postData(recupereNouveauClient())

    }
    else{
        updateData(+btEnregistrerClient.value,recupereNouveauClient())
    }

    // divEnregistrerSucces.style.display = "block"
    
})


    



