"use strict";
let listeClients = [];
let lien = "http://localhost:3000/users";
let lienCommandes = "http://localhost:3000/users/?_embed=commands"; // me donne user et ses commandes de db.json
let lienProduits = "http://localhost:3000/produits";
fetch(lien)
    .then(response => {
    return response.json();
}).then(dataUsers => {
    //console.log(dataUsers)
    for (let leUser of dataUsers) {
        //console.log(leUser.id)
        let leClient = new Client(leUser.id, leUser.nom, leUser.prenom, leUser.email, leUser.siSociete, leUser.Societe, leUser.dateInscription);
        leClient.dateDernierCommande = leUser.dateDernierCommande;
        listeClients.push(leClient);
    }
    afficherTousClients(listeClients);
    ecouteBtAfficherDetail();
    ///// ecoute et modifier client /////
    let listBtModifierClient = document.querySelectorAll("#modifierClient");
    let inputNom = document.querySelector("#nom");
    let inputPrenom = document.querySelector("#prenom");
    let inputEmail = document.querySelector("#email");
    let inputEstSociete = document.querySelector("#estSociete");
    let inputSociete = document.querySelector("#Societe");
    for (let bt of listBtModifierClient) {
        bt.addEventListener("click", (e) => {
            let leClient = recupereClient(e.target.value);
            divAjouterClient.style.display = "block";
            btAjouterClient.style.display = "none";
            btEnregistrerClient.innerHTML = "Modifier";
            btEnregistrerClient.value = bt.value;
            inputNom.value = leClient.nom;
            inputPrenom.value = leClient.prenom;
            inputEmail.value = leClient.email;
            inputSociete.value = leClient.societe;
            if (leClient.siSociete === "off") {
                inputSociete.removeAttribute("required");
                inputSociete.setAttribute("disabled", "");
            }
            else {
                inputEstSociete.setAttribute("checked", "");
                inputSociete.setAttribute("required", "");
                inputSociete.removeAttribute("disabled");
            }
        });
    } /////                     /////
});
//# sourceMappingURL=index.js.map