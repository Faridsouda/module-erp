"use strict";
let tableClients = document.querySelector("#lesClients");
let listNavLinks = document.querySelectorAll(".nav-link");
let lesProduits;
function ecouteBtAfficherDetail() {
    let btSupprimerClient = document.querySelectorAll("#deleteClient");
    for (let bt of btSupprimerClient) {
        bt.addEventListener("click", (e) => {
            deleteData(e.target.value);
        });
    }
    let listeBtAfficherDetail = document.querySelectorAll("#afficherDetailClient");
    for (let bt of listeBtAfficherDetail) {
        bt.addEventListener("click", (e) => {
            tableClients.style.display = "none";
            fetch(lienCommandes)
                .then(response => {
                return response.json();
            }).then(data => {
                afficherDetailClient(e.target.value, data); // value utilisé à la place de dataset car problème avec typescript
            });
            //////
        });
    }
}
for (let link of listNavLinks) { // les links (profil client, commandes et messages)
    link.addEventListener("click", (e) => {
        apparenceLink(e.target, listNavLinks);
        afficherDivLinkClique(e.target);
    });
}
//# sourceMappingURL=ecoute.js.map