"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
let btEnregistrerClient = document.querySelector("#btEnregistrerClient");
let caseSociete = document.querySelector("#estSociete");
function listeTousAchatClientAJour(id, listeObjetsCommandes) {
    let monClient;
    let trouve = false;
    for (let leClient of listeClients) {
        if (leClient.id === id) {
            monClient = leClient;
            trouve = true;
        }
    }
    if (trouve == false)
        throw new Error("Pas de client avec cette id");
    for (let produit of listeObjetsCommandes) {
        monClient.listeTousArticlesIdAchete.push(produit.article_id);
    }
    //console.log("listeObjetsCommandes")
    //console.log(listeObjetsCommandes)
    //console.log("monClient")
    console.log(monClient);
}
let infosProduit = () => {
    let lienInfosArticle = "http://localhost:3000/produits";
    let trouve = false;
    let _obj = {}; // ici est stocker les infos du produit en Object
    let id = 1;
    fetch(lienInfosArticle)
        .then(response => {
        return response.json();
    }).then(data => {
        /*console.log("")
        console.log("data produits")
        console.log(data)*/
        for (let obj of data) {
            if (obj.id === id) {
                trouve = true;
                _obj = obj;
            }
        }
        if (trouve == false)
            throw new Error("l'id ne correspond à aucun produit");
    });
};
function postData(data = {}) {
    return __awaiter(this, void 0, void 0, function* () {
        const response = yield fetch("http://localhost:3000/users", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });
        return response.json();
    });
}
function updateData(id, data = {}) {
    return __awaiter(this, void 0, void 0, function* () {
        const response = yield fetch("http://localhost:3000/users/" + id, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });
        return response.json();
    });
}
function deleteData(id, data = {}) {
    return __awaiter(this, void 0, void 0, function* () {
        const response = yield fetch("http://localhost:3000/users/" + id, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });
        return response.json();
    });
}
function recupereNouveauClient() {
    let nom = document.querySelector("#nom");
    let prenom = document.querySelector("#prenom");
    let email = document.querySelector("#email");
    let societe = document.querySelector("#Societe");
    let nomSociete = "";
    let estSociete;
    if (caseSociete.checked) {
        estSociete = "on";
        if (societe.value == "")
            nomSociete = "non spécifié";
        else
            nomSociete = societe.value;
    }
    else {
        estSociete = "off";
        nomSociete = "/";
    }
    const leNouveauClient = {
        nom: nom.value,
        prenom: prenom.value,
        email: email.value,
        Societe: nomSociete,
        siSociete: estSociete
    };
    return leNouveauClient;
}
btEnregistrerClient.addEventListener("click", () => {
    let divEnregistrerSucces = document.querySelector("#notificationClientEnregistre");
    objetClient();
    if (btEnregistrerClient.innerHTML == "Enregistrer") {
        postData(recupereNouveauClient());
    }
    else {
        updateData(+btEnregistrerClient.value, recupereNouveauClient());
    }
    // divEnregistrerSucces.style.display = "block"
});
//# sourceMappingURL=insertion.js.map