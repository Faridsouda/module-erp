"use strict";
let btAjouterClient = document.querySelector("#ajouterClient");
let divAjouterClient = document.querySelector(".divAjoutClient");
let divProfilClient = document.querySelector("#profilClient");
let divCadreInfoClient = document.querySelector("#cadreInfosClient");
let profilClient = document.querySelector("#profilClient");
let divCommandes = document.querySelector("#commandes");
let divMessages = document.querySelector("#messages");
let tBodyCommandesClient = document.querySelector("#tbodyCommande");
function afficherTousClients(liste) {
    let n = 1;
    for (let LeClient of liste) {
        let tBody = document.querySelector("#lesClients");
        tBody.innerHTML += `
            <tr>
            <th scope="row">${n}</th>
            <td>${LeClient.nom}</td>
            <td>${LeClient.prenom}</td>
            <td>${LeClient.email}</td>
            <td>${LeClient.societe}</td>
            <td><button id="afficherDetailClient" value="${LeClient.id}">Afficher</button></td>
            <td><button id="modifierClient" value="${LeClient.id}">Modifier</button></td>
            <td><button id="deleteClient" value="${LeClient.id}">Supprimer</button></td>
            </tr>`;
        n++;
    }
    console.log("affichage des boutons");
}
function afficherDetailClient(id, data) {
    //console.log(data)
    let _id = +id; // Number
    let listeObjetsCommandes;
    divCadreInfoClient.style.display = "block";
    for (let el of data) { // j'obtiens la liste des articles du clients
        if (+(el.id) == _id) {
            listeObjetsCommandes = el.commands; // et non data.el.commands
        }
    }
    console.log("");
    for (let produit of listeObjetsCommandes) {
        //console.log("article : " + produit.article_id)
    }
    listeTousAchatClientAJour(+id, listeObjetsCommandes);
    afficherProfilClient(_id);
    afficherCommandes(_id);
}
btAjouterClient.addEventListener("click", () => {
    divAjouterClient.style.display = "block";
});
function apparenceLink(nodeLink, listNodeLink) {
    for (let link of listNodeLink) {
        link.className = "nav-link";
    }
    nodeLink.className = "nav-link active";
}
function afficherProfilClient(id) {
    let client = recupereClient(id);
    profilClient.innerHTML += `
    <p></p> 
    <p>Nom : ${client.nom} </p>
    <p>Prénom : ${client.prenom} </p>
    <p>Date inscription : ${client.dateInscription}</p>
    <p>Date dernière commande : ${client.dateDernierCommande}</p>
    `;
}
function afficherDivLinkClique(link) {
    let laSection = link.type;
    if (laSection == "profil") {
        profilClient.style.display = "block";
        divCommandes.style.display = "none";
        divMessages.style.display = "none";
    }
    else if (laSection == "commandes") {
        profilClient.style.display = "none";
        divCommandes.style.display = "block";
        divMessages.style.display = "none";
    }
    else if (laSection == "messages") {
        profilClient.style.display = "none";
        divCommandes.style.display = "none";
        divMessages.style.display = "block";
    }
}
function afficherCommandes(id) {
    let client = recupereClient(id);
    let listCommandes = client.listeTousArticlesIdAchete;
    let n = 1;
    fetch(lienProduits)
        .then(response => {
        return response.json();
    }).then(data => {
        lesProduits = data;
        for (let idd of listCommandes) {
            for (let leProduit of lesProduits) {
                if (idd == leProduit.id) {
                    tBodyCommandesClient.innerHTML += `
                        <tr>
                            <th scope="row">${n}</th>
                            
                            <td>${leProduit.nom}</td>
                            <td>${leProduit.prix} €</td>
                            
                            
                            <td><button id="afficherDetailProduit" value="">Afficher</button></td>
                            
                        </tr>
                        `;
                    n++;
                }
            }
        }
    });
}
//# sourceMappingURL=affichage.js.map