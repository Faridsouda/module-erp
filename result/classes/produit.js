"use strict";
class Produit {
    constructor(id, nom, prix, type = "inconnu") {
        this.id = id;
        this.nom = nom;
        this.prix = prix;
        this.type = type;
    }
}
//# sourceMappingURL=produit.js.map