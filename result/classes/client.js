"use strict";
class Client {
    constructor(id, nom, prenom, email, siSociete = "off", societe = "/", dateInscription) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.siSociete = siSociete;
        this.societe = societe;
        this.dateInscription = dateInscription;
        this.listeTousArticlesIdAchete = [];
        this.dateDernierCommande = "";
    }
}
//# sourceMappingURL=client.js.map